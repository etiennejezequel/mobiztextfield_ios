//
//  CustomTextFieldView.swift
//  Alceane
//
//  Created by Etienne on 09/10/2017.
//Copyright © 2017 Mobizel. All rights reserved.
//

import UIKit

@objc protocol CustomTextFieldViewDelegate {
    @objc optional func didChange(_ textField: CustomTextFieldView)
    @objc optional func verify(text: String, _ textField: CustomTextFieldView)
    @objc optional func dropDown(_ textField: CustomTextFieldView)
    @objc optional func didShowHelp(_ textField: CustomTextFieldView)
    @objc optional func didSelect(_ textField: CustomTextFieldView)
}

enum UITextFieldState {
    case error
    case valid
    case normal
}

class CustomTextFieldView: UIView {

    // MARK: - Publics
    public weak var delegate: CustomTextFieldViewDelegate?
    public var state: UITextFieldState?
    public var helpIsShow: Bool = false
    public var datasKeys: [String] = []
    public var datasValues: [String]  = []
    public var category: String = ""
    public var isPhone: Bool = false
    public var isNumber: Bool = false

    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var vContainer: UIView!
    @IBOutlet fileprivate weak var imgIcon: UIImageView!
    @IBOutlet fileprivate weak var btnShowHelp: UIButton!
    @IBOutlet fileprivate weak var btnShowPwd: UIButton!
    @IBOutlet fileprivate weak var txtfMainText: UITextField!
    @IBOutlet fileprivate weak var viewShowHelp: UIView!
    @IBOutlet fileprivate weak var lblright: UILabel!
    @IBOutlet fileprivate weak var btnRight: UIButton!

    // MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    convenience init(showPwd: Bool = false, showHelp: Bool = false, isPwd: Bool = false, enabled: Bool = true,
                     isDropDown: Bool = false, isNumber: Bool = false, isEmail: Bool = false, isPhone: Bool = false) {
        self.init(frame: CGRect.zero)
        setup()
        self.isPhone = isPhone
        self.isNumber = isNumber
        
        self.btnShowPwd.isHidden = !showPwd
        self.viewShowHelp.isHidden = !showHelp
        if isPwd || showHelp {
            //self.txtfMainText.font = UIFont(font: FontFamily.Montserrat.bold, size: 14)
            self.txtfMainText.textColor = UIColor.black
        }
        
        if isPwd {
            self.txtfMainText.isSecureTextEntry = isPwd
        }
        if !enabled {
            self.diabled()
        }
        if isEmail {
            self.txtfMainText.keyboardType = .emailAddress
        }
        if isDropDown {
            self.btnRight.isHidden = false
            self.btnRight.isHidden = false
            self.txtfMainText.isEnabled = false
            let didTap = UITapGestureRecognizer(target: self, action: #selector(handleAction))
            self.addGestureRecognizer(didTap)
            self.btnRight.addTarget(self, action: #selector(handleAction), for: .touchUpInside)
        }
        if isNumber {
            self.txtfMainText.keyboardType = .decimalPad
        }
        if isPhone {
            self.txtfMainText.keyboardType = .numberPad
        }
    }
}

// MARK: - CustomTextFieldView
extension CustomTextFieldView {
    fileprivate func setup() {
        // Load xib
        guard let nibView = UINib(nibName: "CustomTextFieldView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? UIView else {
            fatalError("Cannot fetch CustomTextFieldView")
        }
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        nibView.frame = bounds
        addSubview(nibView)

        self.setupUI()
    }
    
    fileprivate func setupUI() {
        self.btnRight.isHidden = true
        self.txtfMainText.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.btnRight.setImage(UIImage(named: "triangle"), for: .normal)
        self.btnRight.tintColor = UIColor.green
        self.vContainer.layer.borderWidth = 1
        self.vContainer.layer.borderColor = UIColor.black.cgColor
        self.vContainer.layer.cornerRadius = 2
        self.btnShowPwd.addTarget(self, action: #selector(handleShowPwdAction), for: .touchUpInside)
        
        self.imgIcon.contentMode = .center
        self.imgIcon.backgroundColor = UIColor.gray
        self.btnShowHelp.setImage(UIImage(named: "pictoHelp"), for: .normal)
        self.btnShowPwd.setImage(UIImage(named: "pictoView"), for: .normal)
        self.btnShowHelp.addTarget(self, action: #selector(handleTapShowHelp), for: .touchUpInside)
        self.txtfMainText.borderStyle = .none
        //self.txtfMainText.font = UIFont(font: FontFamily.Montserrat.bold, size: 14)
        self.txtfMainText.textColor = UIColor.black
        self.txtfMainText.delegate = self
        self.lblright.textAlignment = .center
        //self.lblright.font = UIFont(font: FontFamily.ProximaNova.bold, size: 16)
        self.lblright.backgroundColor = UIColor.gray
        self.lblright.textColor = UIColor.black
        
        self.applyContent()
    }
    
    @objc private func handleShowPwdAction() {
        self.txtfMainText.isSecureTextEntry = !self.txtfMainText.isSecureTextEntry
    }
    
    fileprivate func diabled() {
        self.txtfMainText.isEnabled = false
        self.vContainer.backgroundColor = UIColor.white
        self.txtfMainText.text = self.txtfMainText.placeholder
    }
    
    @objc fileprivate func handleTapShowHelp() {
        self.delegate?.didShowHelp?(self)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.delegate?.didChange?(self)
    }
    
    @objc fileprivate func handleAction() {
        self.delegate?.dropDown?(self)
    }
    
    public func applyContent(img: UIImage? = nil, placeHolder: String? = nil, secondImgPath: String? = nil) {
        if let placeHolder = placeHolder {
            self.txtfMainText.placeholder = placeHolder
        } else {
            self.txtfMainText.placeholder = ""
        }
        
        if let img = img {
            self.imgIcon.image = img
            self.imgIcon.isHidden = false
        } else {
            self.imgIcon.isHidden = true
        }
        
        if let secondImgName = secondImgPath, !secondImgName.isEmpty {
            self.lblright.text = secondImgName
            self.lblright.isHidden = false
        } else {
            self.lblright.isHidden = true
        }
    }
    
    public func getTextFieldValue() -> String {
        if let value = self.txtfMainText.text {
            return value
        } else {
            return ""
        }
    }
    
    public func setColor(state: UITextFieldState) {
        self.state = state
        switch state {
        case .error:
            self.vContainer.layer.borderColor = UIColor.red.cgColor
        case .valid:
            self.vContainer.layer.borderColor = UIColor.green.cgColor
        case .normal:
            self.vContainer.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    public func setTextFieldValue(_ value: String) {
        self.txtfMainText.text = value
    }
    
    public func setTextFieldFont(_ value: UIFont, _ color: UIColor) {
        self.txtfMainText.textColor = color
        self.txtfMainText.font = value
    }
    
    public func resetTextField() {
        self.setTextFieldValue("")
        self.vContainer.layer.borderColor = UIColor.black.cgColor
    }
    
    public func getHelpButton() -> UIButton {
        return self.btnShowHelp
    }
    
    public func isValid() -> Bool {
        return self.state == UITextFieldState.valid
    }
    
    public func isEmpty() -> Bool {
        return self.txtfMainText.text?.isEmpty ?? true
    }
}

extension CustomTextFieldView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.verify?(text: self.getTextFieldValue(), self)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if isPhone {
            guard let text = self.txtfMainText.text else { return true }
            if string.isNumber || string.isEmpty {
                let newLength = text.count + string.count - range.length
                return newLength <= 10
            } else {
                return false
            }
        }
        if isNumber {
            let suffix = self.txtfMainText.text?.suffix(3).first
            if suffix != nil, suffix == "," || suffix == ".", !string.isEmpty {
                return false
            }
            return string.isNumber || string == "," || string == "." || string.isEmpty
        }
        return true
    }
}
