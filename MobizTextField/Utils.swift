//
//  Utils.swift
//  MobizTextField
//
//  Created by Etienne Jezequel on 11/01/2018.
//  Copyright © 2018 Mobizel. All rights reserved.
//

import Foundation

extension String {
    
    var isNumber: Bool {
        return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}
