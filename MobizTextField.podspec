Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "MobizTextField"
s.summary = "MobizTextField is a customisable textfield"
s.requires_arc = true
s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }

# 2
s.version = "0.1.3"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Etienne Jézéquel" => "etienne@mobizel.com" }

# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "https://bitbucket.org/etiennejezequel/mobiztextfield_ios/"

# For example,
# s.homepage = "https://github.com/JRG-Developer/RWPickFlavor"


# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://etiennejezequel@bitbucket.org/etiennejezequel/mobiztextfield_ios.git", :tag => "#{s.version}"}

# For example,
# s.source = { :git => "https://github.com/JRG-Developer/RWPickFlavor.git", :tag => "#{s.version}"}


# 7
s.framework = "UIKit"

# 8
s.source_files = "MobizTextField/**/*.{swift}"

# 9
s.resources = "MobizTextField/**/*.{png,jpeg,jpg,storyboard,xib}"
end
